﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace WEBMobileFoodSchedule.Models
{
    public static class FoodTrucksRepository
    {
        public static List<FoodTrucks> GetListFoodTrucks(string day, string time)
        {
            try
            {
                var client = new RestClient("https://data.sfgov.org/resource/jjew-r69b.json?" + "$where='" + time + "' between start24 and end24 and dayofweekstr = '" + day + "'");
                client.Timeout = -1;
                var request = new RestRequest(Method.GET);
                IRestResponse response = client.Execute(request);
                return response.StatusCode == HttpStatusCode.OK && !string.IsNullOrEmpty(response.Content)
                    ? JsonConvert.DeserializeObject<List<FoodTrucks>>(response.Content)
                    : null;
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }
}