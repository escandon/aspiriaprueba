﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WEBMobileFoodSchedule.Models
{
    public class FoodTrucks
    {
        public string dayorder { get; set; }
        public string dayofweekstr { get; set; }
        public string starttime { get; set; }
        public string endtime { get; set; }
        public string permit { get; set; }
        public string location { get; set; }
        public string locationdesc { get; set; }
        public string optionaltext { get; set; }
        public string locationid { get; set; }
        public string start24 { get; set; }
        public string end24 { get; set; }
        public string cnn { get; set; }
        public string addr_date_create { get; set; }
        public string addr_date_modified { get; set; }
        public string block { get; set; }
        public string lot { get; set; }
        public string coldtruck { get; set; }
        public string applicant { get; set; }
    }
}