﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WEBMobileFoodSchedule.Models;

namespace WEBMobileFoodSchedule.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetListFoodTrucks(string day, string time)
        {
            var data = FoodTrucksRepository.GetListFoodTrucks(day,time);
            data = data.OrderBy(x => x.applicant).ToList();
            return PartialView(data);

        }

    }
}