USE [master]
GO
/****** Object:  Database [DBAspiria]    Script Date: 14/08/2021 03:38:57 p. m. ******/
CREATE DATABASE [DBAspiria]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'DBAspiria', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\DBAspiria.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'DBAspiria_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\DBAspiria_log.ldf' , SIZE = 2048KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [DBAspiria] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [DBAspiria].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [DBAspiria] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [DBAspiria] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [DBAspiria] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [DBAspiria] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [DBAspiria] SET ARITHABORT OFF 
GO
ALTER DATABASE [DBAspiria] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [DBAspiria] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [DBAspiria] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [DBAspiria] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [DBAspiria] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [DBAspiria] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [DBAspiria] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [DBAspiria] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [DBAspiria] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [DBAspiria] SET  DISABLE_BROKER 
GO
ALTER DATABASE [DBAspiria] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [DBAspiria] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [DBAspiria] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [DBAspiria] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [DBAspiria] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [DBAspiria] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [DBAspiria] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [DBAspiria] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [DBAspiria] SET  MULTI_USER 
GO
ALTER DATABASE [DBAspiria] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [DBAspiria] SET DB_CHAINING OFF 
GO
ALTER DATABASE [DBAspiria] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [DBAspiria] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [DBAspiria] SET DELAYED_DURABILITY = DISABLED 
GO
USE [DBAspiria]
GO
/****** Object:  Table [dbo].[Juguetes]    Script Date: 14/08/2021 03:38:57 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Juguetes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[Descripcion] [varchar](100) NULL,
	[RestriccionEdad] [int] NULL,
	[Compania] [varchar](50) NOT NULL,
	[Precio] [decimal](18, 0) NOT NULL,
 CONSTRAINT [PK_Juguetes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Juguetes] ON 

INSERT [dbo].[Juguetes] ([Id], [Nombre], [Descripcion], [RestriccionEdad], [Compania], [Precio]) VALUES (1, N'123', N'Prueba', 11, N'123', CAST(10 AS Decimal(18, 0)))
INSERT [dbo].[Juguetes] ([Id], [Nombre], [Descripcion], [RestriccionEdad], [Compania], [Precio]) VALUES (2, N'Prueba2', N'Prueba2', 12, N'Prueba2', CAST(9 AS Decimal(18, 0)))
INSERT [dbo].[Juguetes] ([Id], [Nombre], [Descripcion], [RestriccionEdad], [Compania], [Precio]) VALUES (3, N'123', N'123', 1, N'123', CAST(1 AS Decimal(18, 0)))
INSERT [dbo].[Juguetes] ([Id], [Nombre], [Descripcion], [RestriccionEdad], [Compania], [Precio]) VALUES (7, N'qwe', N'qwe', 0, N'0', CAST(0 AS Decimal(18, 0)))
INSERT [dbo].[Juguetes] ([Id], [Nombre], [Descripcion], [RestriccionEdad], [Compania], [Precio]) VALUES (8, N'123', N'21111', 123, N'123', CAST(123 AS Decimal(18, 0)))
INSERT [dbo].[Juguetes] ([Id], [Nombre], [Descripcion], [RestriccionEdad], [Compania], [Precio]) VALUES (10, N'111', N'1111', 111, N'11', CAST(11 AS Decimal(18, 0)))
SET IDENTITY_INSERT [dbo].[Juguetes] OFF
USE [master]
GO
ALTER DATABASE [DBAspiria] SET  READ_WRITE 
GO
