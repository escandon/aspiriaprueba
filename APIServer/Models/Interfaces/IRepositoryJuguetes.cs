﻿using APIServer.Models.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APIServer.Models.Interfaces
{
    public interface IRepositoryJuguetes
    {
        Task<List<JuguetesModel>> GetListJuguetes();
        Task<JuguetesModel> GetJuguete(int id);
        Task<int> CreateJuguete(JuguetesModel JuguetesModel);
        Task<int> EditJuguete(JuguetesModel JuguetesModel);
        Task<int> DeleteJuguete(int id);

    }
}
