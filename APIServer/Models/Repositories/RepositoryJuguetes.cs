﻿using APIServer.Models.DTOs;
using APIServer.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace APIServer.Models.Repositories
{
    public class RepositoryJuguetes : IRepositoryJuguetes
    {
        public Task<int> CreateJuguete(JuguetesModel JuguetesModel)
        {
            using (var db = new DBAspiriaEntities())
            {
                var Juguetes = new Juguetes();
                Juguetes.Id = 0;
                Juguetes.Nombre = JuguetesModel.Nombre;
                Juguetes.Descripcion = JuguetesModel.Descripcion;
                Juguetes.RestriccionEdad = JuguetesModel.RestriccionEdad;
                Juguetes.Compania = JuguetesModel.Compania;
                Juguetes.Precio = JuguetesModel.Precio;
                db.Juguetes.Add(Juguetes);
                db.SaveChanges();

                return Task.FromResult(Juguetes.Id);
            }
        }

        public Task<int> DeleteJuguete(int id)
        {
            using (var db = new DBAspiriaEntities())
            {
                var Juguetes = db.Juguetes.Find(id);
                db.Juguetes.Remove(Juguetes);
                db.SaveChanges();

                return Task.FromResult(id);
            }
        }

        public Task<int> EditJuguete(JuguetesModel JuguetesModel)
        {
            using (var db = new DBAspiriaEntities())
            {
                var Juguetes = db.Juguetes.Find(JuguetesModel.Id);
                Juguetes.Nombre = JuguetesModel.Nombre;
                Juguetes.Descripcion = JuguetesModel.Descripcion;
                Juguetes.RestriccionEdad = JuguetesModel.RestriccionEdad;
                Juguetes.Compania = JuguetesModel.Compania;
                Juguetes.Precio = JuguetesModel.Precio; db.SaveChanges();

                return Task.FromResult(Juguetes.Id);
            }
        }

        public Task<JuguetesModel> GetJuguete(int id)
        {
            using (var db = new DBAspiriaEntities())
            {
                var Juguetes = db.Juguetes.Find(id);
                return Task.FromResult(new JuguetesModel
                {
                    Id = Juguetes.Id,
                    Nombre = Juguetes.Nombre,
                    Descripcion = Juguetes.Descripcion,
                    RestriccionEdad = Juguetes.RestriccionEdad ?? 0,
                    Compania = Juguetes.Compania,
                    Precio = Juguetes.Precio
                });
            }
        }

        public Task<List<JuguetesModel>> GetListJuguetes()
        {
            using (var db = new DBAspiriaEntities())
            {
                var Juguetes = db.Juguetes.Select(s => new JuguetesModel
                {
                    Id = s.Id,
                    Nombre = s.Nombre,
                    Descripcion = s.Descripcion,
                    RestriccionEdad = s.RestriccionEdad ?? 0,
                    Compania = s.Compania,
                    Precio = s.Precio,
                }).ToList();
                return Task.FromResult(Juguetes);
            }
        }
    }
}