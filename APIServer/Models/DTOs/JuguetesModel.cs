﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace APIServer.Models.DTOs
{
    public class JuguetesModel
    {
        [DisplayName("Id")]
        [Required(ErrorMessage = "El campo es requerido")]
        public int Id { get; set; }

        [DisplayName("Nombre")]
        [StringLength(50, ErrorMessage = "Maximo 50 caracteres")]
        [Required(ErrorMessage = "El campo es requerido")]
        public string Nombre { get; set; }

        [DisplayName("Descripcion")]
        [StringLength(100, ErrorMessage = "Maximo 100 caracteres")]
        public string Descripcion { get; set; }

        [DisplayName("RestriccionEdad")]
        [Range(0, 100, ErrorMessage = "Seleccione un número entre 0 y 100")]
        public int RestriccionEdad { get; set; }

        [DisplayName("Compañia")]
        [StringLength(50, ErrorMessage = "Maximo 50 caracteres")]
        [Required(ErrorMessage = "El campo es requerido")]
        public string Compania { get; set; }

        [DisplayName("Precio")]
        [Required(ErrorMessage = "El campo es requerido")]
        public decimal Precio { get; set; }
    }
}