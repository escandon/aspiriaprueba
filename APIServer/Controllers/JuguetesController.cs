﻿using APIServer.Models.DTOs;
using APIServer.Models.Interfaces;
using APIServer.Models.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;


namespace APIServer.Controllers
{
    [RoutePrefix("api")]
    public class JuguetesController : ApiController
    {
        private readonly IRepositoryJuguetes _IRepositoryJuguetes;

        public JuguetesController()
        {
            _IRepositoryJuguetes = new RepositoryJuguetes();
        }

        [HttpGet]
        [Route("GetListJuguetes")]
        public async Task<IHttpActionResult> GetListJuguetes()
        {
            try
            {
                return Ok(new { msg = "ok", data = await _IRepositoryJuguetes.GetListJuguetes() });
            }
            catch (Exception e)
            {
                return Ok(new { msg = e.Message });
            }
        }

        [HttpGet]
        [Route("GetJuguete")]
        public async Task<IHttpActionResult> GetJuguete([FromUri] int id)
        {
            try
            {
                return Ok(new { msg = "ok", data = await _IRepositoryJuguetes.GetJuguete(id) });
            }
            catch (Exception e)
            {
                return Ok(new { msg = e.Message });
            }
        }


        [HttpPost]
        [Route("CreateJuguete")]
        public async Task<IHttpActionResult> CreateJuguete([FromBody]JuguetesModel juguetesModel)
        {
            try
            {
                return Ok(new { msg = "ok", data = await _IRepositoryJuguetes.CreateJuguete(juguetesModel) });
            }
            catch (Exception e)
            {
                return Ok(new { msg = e.Message });
            }
        }

        [HttpPut]
        [Route("EditJuguete")]
        public async Task<IHttpActionResult> EditJuguete([FromBody] JuguetesModel juguetesModel)
        {
            try
            {
                return Ok(new { msg = "ok", data = await _IRepositoryJuguetes.EditJuguete(juguetesModel) });
            }
            catch (Exception e)
            {
                return Ok(new { msg = e.Message });
            }
        }

        [HttpDelete]
        [Route("DeleteJuguete")]
        public async Task<IHttpActionResult> DeleteJuguete([FromUri]int id)
        {
            try
            {
                return Ok(new { msg = "ok", data = await _IRepositoryJuguetes.DeleteJuguete(id) });
            }
            catch (Exception e)
            {
                return Ok(new { msg = e.Message });
            }
        }
    }
}